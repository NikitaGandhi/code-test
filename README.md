# Filter iphones
This application is writen in AngularJs that filters iPhones based on 'color' and 'capacity' fields.

# How to Run
Open index.html in any browser and star typing search term in given textbox that updates the iPhone table based on search string.

e.g. if I use the search term 'Gold' this should return true for iphones that have a color value of 'Rose Gold' and 'Gold', if I use the search term '64' this should return true for iphones where the capacity is value of '64GB'

# How to Run Test
For unit test this applications uses karma-jasmine plugins.

### Dependency - Install Node.js:
There are node.js(https://nodejs.org/en/download/) for both Mac and Windows.

### Install karma:
cd path/to/code-test
npm install karma --save-dev
npm install -g karma-cli

### Install karma-jasmine:
npm install karma-jasmine karma-chrome-launcher --save-dev

### Run Karma:
karma start
