describe('filterPhoneCtrl', function() {
  beforeEach(angular.mock.module('filterPhone'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('$scope.doFilter', function() {
    var $scope, controller, iphone;

    beforeEach(function() {
      iphone = {
            "id": 1,
            "name": "iPhone SE",
            "color": "Silver",
            "type": "Wifi",
            "capacity": "16GB",
            "price": 629
      };
      $scope = {};
      controller = $controller('filterPhoneCtrl', { $scope: $scope });
    });

    it('returns true if iphone color is silver and searchTerm is set to silver', function() {
      $scope.searchTerm = 'silver';

      expect($scope.doFilter(iphone)).toEqual(true);
    });

    it('returns false if iphone color is silver and searchTerm is set to gold', function() {
      $scope.searchTerm = 'gold';

      expect($scope.doFilter(iphone)).toEqual(false);
    });

    it('returns false if iphone capacity is 16GB and searchTerm is set to 16', function() {
      $scope.searchTerm = '16';

      expect($scope.doFilter(iphone)).toEqual(true);
    });

    it('returns false if iphone capacity is 16GB and searchTerm is set to 64', function() {
      $scope.searchTerm = '64';

      expect($scope.doFilter(iphone)).toEqual(false);
    });

  });
});
